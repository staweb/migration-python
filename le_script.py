from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
#from googleapiclient import discovery
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

import json
import csv
import copy

# If modifying these scopes, delete the file token.pickle.
#SCOPES = ['https://www.googleapis.com/auth/drive.readonly']
SCOPES = ['https://www.googleapis.com/auth/drive']
#ROOTNAME = "STA"
ROOTNAME = "STA"

FOLDER = 'application/vnd.google-apps.folder'
FORM = 'application/vnd.google-apps.form'
PDF = 'application/pdf'
DRAWING = 'application/vnd.google-apps.drawing'
SPREADSHEET = 'application/vnd.google-apps.spreadsheet'
DOCUMENT = 'application/vnd.google-apps.document'
PRESENTATION = 'application/vnd.google-apps.presentation'

def main():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    #Can we take this bit out to a function?
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=8080)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)
    docs_service = build('docs', 'v1', credentials=creds)

    #--------- THE ACTUAL PART THAT WE ARE CONCERNED ABOUT ------------------

    # Call the Drive v3 API
    
    
    #email = "google-drive-archive@my.bristol.ac.uk"
    #email = "greg.quick@bristolsta.com"
    email = "@bristol.ac.uk"
    results = retrieve_all_files(service, email)

    print("email: " + email)
   
    # Get list of already completed files
    #oldf = open("old_staff_docs.csv", "r")

    #completed = []
    #csv_reader = csv.DictReader(oldf, delimiter=',')
    #for line in csv_reader:
    #    print(line['id'])
    #    completed.append(line['id'])
    #print("The already completed files are: ")
    #print(completed)

    #oldf.close()
    
    f = open("staff_folders.csv", "w")
   
    #
    #for item in results:
    #    if item['mimeType'] == FOLDER:
    #        exampleItem = copy.deepcopy(results[0])
    #        break
    exampleItem = copy.deepcopy(results[0])
    exampleItem['itemType'] = ""
    exampleItem['newFileCreated'] = ""
    exampleItem['childrenCopied'] = ""
    exampleItem['renameSucess'] = ""
    exampleItem['removedParent'] = ""
    exampleItem['owners'] = ""
    exampleItem['size'] = ""
    exampleItem['email'] = ""


    keys = exampleItem.keys()
    writer = csv.DictWriter(
        f, keys
    )

    writer.writeheader()

    #if len(completed) != 12:
    #    print("compled is wrong!")
    #    return 
    # DONT FORGET ELIF if you uncomment above
    if not results:
        print('No files found.')
    else:
        print('Files:')
        for item in results:
            #if item['mimeType'] != FOLDER:
            #    # if item['mimeType'] != SPREADSHEET:
            #    if item['mimeType'] != DOCUMENT and item['mimeType'] != SPREADSHEET and item['mimeType'] != PRESENTATION and item['mimeType'] != FORM and item['mimeType'] != DRAWING:
            #        #if ('image' in item['mimeType']) or ('video' in item['mimeType']):
            #        #    print("file is image or video")
            #        ##print("Copying file with name" + item['name'])
            #        #else:
            #        print("Checking file with name " + item['name'])
            #        print(item)

            #        if item['id'] in completed:
            #            print("This file was completed before")
            #            write_item_csv(writer, item, "Previous Run", False)
            #        else:
            #            copySuccess, renameSucess = copy_file(service, item)
            #            write_item_csv(writer, item, copySuccess, renameSucess)

            #        print()
            #        #print("File copy completed")
            #        print("File check completed")
            #        #write_item_csv(writer, item, "Script", renameSucess, copyComments)
            #        #write_item_csv(writer, item, "Script", renameSucess, copyComments, commentCount, totalCount)
            #        print(item)
            #        print("-----------")
            #        print()
            #if item['id'] in completed:
            #    print("SKIPPING: Complted before")
            #    write_item_csv(writer, item, "Skipped as completed", False, False, -1, -1, "n/a")
            #else:
            for owner in item['owners']:
                if item['mimeType'] == FOLDER:
                    if email in owner['emailAddress']:
                        print()
                        print("Copying file with name" + item['name'])
                        newFolderCreate, childrenCopied, renameSucess, allParentsRemoved = copy_folder(service, item, email, writer, results)
                        write_item_csv(writer, "FOLDER", item, newFolderCreate, childrenCopied, renameSucess, allParentsRemoved, owner['emailAddress'])

                        print(owner)
                        #print("Copying folder with name" + item['name'])
                        #newFolderCreate, childrenCopied, renameSucess, allParentsRemoved = copy_folder(service, item, email, writer, results)
                        #write_item_csv(writer, "FOLDER", item, newFolderCreate, childrenCopied, renameSucess, allParentsRemoved)
                        print(item)
                        print("-----------")
                        print()


                
def update_results_parent(movedFolderId, oldParentId, newParentId, results):
    for result in results:
        if result['id'] == movedFolderId:
            print("Updating parent info in results, having moved the folder")
            print("Original parents were: ")
            print(result['parents'])
            result['parents'].remove(oldParentId) 
            result['parents'].append(newParentId) 
            print("New parents are: ")
            print(result['parents'])

# TODO
def copy_folder(api_service, folder, email, writer, results):
    allChildrenCopied = True
    renameSucess = True
    allParentsRemoved = True
    try:
        newFileId = create_folder(api_service, folder)
    except:
        return False, False, False, False
    try:
        rename_file(api_service, folder)
    except:
        renameSucess = False 
    children = find_children(api_service, folder['id'])
    for child in children:
        print()
        print("Migration child " + child['name'] + " to new folder")
        print(child)
        if owned_by_email(api_service, child, email) and child['mimeType'] != FOLDER:
            print("Ignored file as owned by archive")
            write_item_csv(writer, "CHILD", child, "SKIPED as owned by Archive", "n/a", "n/a", False, "n/a")
        else:
            print("Updating Parents of: " + child['name'])
            addedNewParent = add_parent(api_service, child['id'], newFileId)
            if addedNewParent:
                removed_parent = remove_parent(api_service, child['id'], folder['id'])
                if remove_parent:
                    print("The old parent was successfuly removed")
                else:
                    allParentsRemoved = False
                    print("ERROR: Failed to remove parent of this child")
                write_item_csv(writer, "CHILD", child, "COPIED child", "n/a", "n/a", removed_parent, "n/a")
                print("Successfully updated parent")
                print("Updating results")
                update_results_parent(child['id'], folder['id'], newFileId, results)
            else:
                allChildrenCopied = False
                allParentsRemoved = False
                write_item_csv(writer, "CHILD", child, "FAILED to copy child", "n/a", "n/a", False, "n/a")
                print("FAILED to copy child")
        print("Migration of child " + child['name'] + " complete")
        print()
    return True, allChildrenCopied, renameSucess, allParentsRemoved


def owned_by_email(api_service, child, email):
   owners = child.get('owners')
   for owner in owners:
       if email in owner.get('emailAddress'):
           return True
   return False


def create_folder(api_service, folder):
    newFolder = {
            'mimeType': folder['mimeType'],
            'name': folder['name'], 
            'parents': folder['parents'],
    }
    createResponse = api_service.files().create(body=newFolder, fields="id").execute()
    return createResponse['id']


#return list of children of give folder
def find_children(api_service, folderId):
    return retrieve_all_children(api_service, folderId)

# TODO
def add_parent(api_service, fileId, parentId):
    try: 
        api_service.files().update(fileId=fileId, addParents=parentId).execute()
        return True
    except: 
        return False

def remove_parent(api_service, fileId, parentId):
    try:
        api_service.files().update(fileId=fileId, removeParents=parentId).execute()
        return True
    except:
        return False

def has_suggestions(docs_service, item):
    document = docs_service.documents().get(documentId=item['id']).execute()
    return dict_has_suggestions(document)

def dict_has_suggestions(dictionary):
    if (isinstance(dictionary, dict)):
        if(contains_suggested(dictionary.keys())):
            return True
        for key in dictionary:
            if dict_has_suggestions(dictionary[key]): 
                return True
    elif (isinstance(dictionary, list)):
        for item in dictionary: # Note that dictionary is a list xD
            if dict_has_suggestions(item):
                return True
    return False

def contains_suggested(keys):
    if "suggested" in str(keys):
        return True
    return False


def time_to_time(time):
    year = time[:4]
    month = time[5:7]
    day = time[8:10]
    hour = time[11:13]
    minute = time[14:16]

    return (hour + ":" + minute + " on " + day + "/" + month + "/" + year)

def has_comments(api_service, fileId):
    try:
        commentsResponse = api_service.comments().list(fileId=fileId, fields="*").execute()
        print("Comment response")
    except:
        print("ERROR whilst trying to get comments")
        return False
    if len(commentsResponse['comments']) > 0:
        return True 
    return False

def number_of_comments(api_service, fileId):
    try:
        comments = retrieve_all_comments(api_service, fileId)
    except:
        return 0
    return len(comments)

def rename_file(api_service, fileData):
    try:
        api_service.files().update(fileId=fileData['id'], body={'name':'ARCHIVE_COPY of ' + fileData['name']}).execute()
        print("Renamed the original file")
        return True
    except:
        print("Warning! Failed to rename file: " )
        print(fileData)
        return False

def write_item_csv(writer, itemType, item, newFileCreated, childrenCopied, renameSucess, removedParent, email):
    item['itemType'] = itemType
    item['newFileCreated'] = newFileCreated
    item['childrenCopied'] = childrenCopied
    item['renameSucess'] = renameSucess
    item['removedParent'] = removedParent
    item['email'] = removedParent
    writer.writerow(item)

def copy_file_withcomment(api_service, fileData):
    renameSucess = False
    copyComments = False

    fileId = fileData['id']
    print("Copying the base file")
    newFileFields = api_service.files().copy(fileId = fileId, keepRevisionForever=True, fields="id", body={'name': fileData['name']}).execute()
    print("File copied")
   
    fileType = fileData['mimeType']
   
    # Try renaming the file
    try:
        api_service.files().update(fileId=fileData['id'], body={'name':'ARCHIVE_COPY of ' + fileData['name']}).execute()
        print("Renamed the original file")
        renameSucess = True
    except:
        print("Warning! Failed to rename file: " )
        print(fileData)

    copyComments, commentcount, totalCount = copy_comments(api_service, fileData, newFileFields['id'])
    return renameSucess, copyComments, commentcount, totalCount

def copy_file(api_service, fileData):
    renameSucess = False

    fileId = fileData['id']
    print("Copying the base file")
    try:
        newFileFields = api_service.files().copy(fileId = fileId, keepRevisionForever=True, fields="id", body={'name': fileData['name']}).execute()
    except:
        print("ERROR: Failed to copy file")
        return "FAILED", False
    print("File copied")
   
    #fileType = fileData['mimeType']
   
    # Try renaming the file
    try:
        api_service.files().update(fileId=fileData['id'], body={'name':'ARCHIVE_COPY of ' + fileData['name']}).execute()
        print("Renamed the original file")
        renameSucess = True
    except:
        print("Warning! Failed to rename file: " )
        print(fileData)

    #if fileType == DOCUMENT:
    #    copyComments, commentcount, totalCount = copy_comments(api_service, fileData, newFileFields['id'])
    #    return renameSucess, copyComments, commentcount, totalCount

    #print("Apparently the doc type is DOCUMENT")
    return True, renameSucess
   
def copy_comments(api_service, templateFileData, newFile):
    commentcount = 0
    totalCount = 0

    templateFile = templateFileData['id']
    print("Copying comments...")

    # In try as some files do not support comments
    try:
        comments = retrieve_all_comments(api_service, templateFileData['id']);
        commentcount = len(comments)
        totalCount = commentcount
    #commentsResponse = api_service.comments().list(pageSize=100, fileId=templateFile, fields="*").execute()
        print(comments)
    # If failed return to copy without comments
    except:
        print("ERROR: Exception when try to get comments")
        return False, -1, -1

    # For all the comments
    #if not commentsResponse['comments']:
    if not comments:
        print("WARNING: No comments returned")
        return False, 0, 0
    else:
        for comment in comments:
            # Get all the replies to add later
            replies = comment['replies']
            totalCount += len(replies)
            # Then remove them from the dictionary because google get sad if you try and add then during creation
            del comment['replies']
            author = comment['author']
            comment['content'] = "Comment created by: " + author['displayName'] + "\n" + "At: " + time_to_time(comment['createdTime']) + "\n" + comment['content']
            comment['htmlContent'] = "Comment created by: " + author['displayName'] + "<br>" + "At: " + time_to_time(comment['createdTime']) + "<br>" + comment['htmlContent']
            newCommentResponse = api_service.comments().create(fileId=newFile, body=comment, fields="id").execute()

            # Copy the comment it self
            newCommentId = newCommentResponse['id']
            # Then copy the replies
            for reply in replies: 
                author = reply['author']
                reply['content'] = "Comment created by: " + author['displayName'] + "\n" + "At: " + time_to_time(reply['createdTime']) + "\n" + reply['content']
                reply['htmlContent'] = "Comment created by: " + author['displayName'] + "<br>" + "At: " + time_to_time(reply['createdTime']) + "<br>" + reply['htmlContent']
                api_service.replies().create(fileId=newFile, commentId=newCommentId, body=reply, fields="id").execute()

    print("Comments copied")
    return True, commentcount, totalCount

def retrieve_all_comments(api_service, fileId):
    comments = []
    page_token = None

    print("Getting all the comments")
    while True:
        commentsResponse = api_service.comments().list(pageToken=page_token, pageSize=100, fileId=fileId, fields="*").execute()
        try:
            comments.extend(commentsResponse.get('comments'))
            page_token = commentsResponse.get('nextPageToken')
            if not page_token:
                break

        except:
            print('WARNING: Error occured whilst accessing page of comments')
            break

    return comments

# define a function to retrieve all files
def retrieve_all_files(api_service, email):
    results = []
    page_token = None

    print("Getting all the files")
    while True:
        try:
            #files = api_service.files().list(pageToken=page_token, corpora="allDrives", includeItemsFromAllDrives=True, supportsAllDrives=True, pageSize=1000, fields="nextPageToken, files(id, name, parents, size, mimeType)", q = "'" + email + "' in owners").execute()
            #files = api_service.files().list(pageToken=page_token, pageSize=1000, fields="nextPageToken, files(id, name, parents, size, mimeType)", q = "'" + email + "' in owners").execute()
            files = api_service.files().list(pageToken=page_token, pageSize=1000, fields="nextPageToken, files(id, name, parents, size, mimeType, owners)").execute()
            #files = api_service.files().list(pageToken=page_token, pageSize=1000, fields="*", q = "'" + email + "' in owners").execute()
# append the files from the current result page to our list
            results.extend(files.get('files'))
# Google Drive API shows our files in multiple pages when the number of files exceed 100
            page_token = files.get('nextPageToken')

            if not page_token:
                break

        # Lol this is an accident waiting to happen
        except:
            print('An error has occurred whilst getting all the files')
            break

    return results

def retrieve_all_children(api_service, parentId):
    results = []
    page_token = None

    print("Getting all the children of: " + parentId)
    while True:
        files = api_service.files().list(pageToken=page_token, pageSize=1000, fields="nextPageToken, files(id, name, parents, size, mimeType, owners)", q = "'" + parentId + "' in parents").execute()
        results.extend(files.get('files'))
        page_token = files.get('nextPageToken')

        if not page_token:
            break

        #except errors.HttpError as error:
        #    print('An error has occurred: {error}')
        #    break
    return results

#def copy_

# Update owner email -> do not have permission
def update_owner(api_service, fileData):
    new_owner_email = "gq14119@my.bristol.ac.uk"

    new_permission = {
        'type': 'user',
        'role': 'owner',
        'emailAddress': new_owner_email
    }
    service.permissions().create(fileId = item['id'], body=new_permission, transferOwnership=True).execute()
    
    permissionResponse = api_service.permissions().list(fileId = fileData['id'], fields="*").execute()
    #print(permissionResponse) 
    for permission in permissionResponse['permissions']:
        if not 'emailAddress' in permission:
            print("Doesnt have an email address")
        else: 
            if permission['emailAddress'] == new_owner_email:
                print(permission)
                new
                permission['role'] = 'owner'
                #permission = api_service.permissions().update(fileId = fileData['id'], permissionId=permission['id'], body=permission, transferOwnership=True).execute()
                permission = api_service.permissions().create(fileId = fileData['id'], body=permission, transferOwnership=True).execute()


if __name__ == '__main__':
    main()

   
